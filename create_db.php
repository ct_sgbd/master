<?php
    include("assets/includes/header.php")
?>

<div class="container">
    <div class="row">
        <div class="col s4" id="jstree"><?php listFolderFiles("db") ?></div>

        <div class="col s8" id="add_new_db">
            <h3>Create new database</h3>

            <input id="db_name" class="form-control" placeholder="Database name">
            <button id="db_add" class="btn btn-success right">Create database</button>

        </div>
    </div>
</div>

<?php
    include("assets/includes/footer.php")
?>