<?php
    include("assets/includes/header.php");
?>

<div class="container">
    <div class="row" >
        <div class="col s4" id="jstree"><?php listFolderFiles("db") ?></div>

        <div class="col s8 container" id="add_new_table">
            <input id="db_name" value="<?= $_GET["db"]; ?>" style="display: none;">
            <h3>Create new table</h3>

            <input id="table_name" type="text" class="validate" placeholder="Table name">

            <div class="row table_row">
                <div class="input-field col s2">
                    <input type="text" class="validate name" placeholder="Name">
                </div>
                <div class="input-field col s2">
                    <select class="type">
                        <option disabled selected>Type</option>
                        <option value="INTEGER">INTEGER</option>
                        <option value="VARCHAR">VARCHAR</option>
                    </select>
                </div>
                <div class="input-field col s2">
                    <input type="text" class="validate length" placeholder="Length">
                </div>
                <div class="input-field col s2">
                    <select class="default">
                        <option disabled selected>Default</option>
                        <option value="NOT NULL">NOT NULL</option>
                        <option value="NULL">NULL</option>
                    </select>
                </div>
                <div class="input-field col s2">
                    <select class="extra">
                        <option disabled selected>Extra</option>
                        <option value="NONE">None</option>
                        <option value="PRIMARY KEY">Primary key</option>
                        <option value="UNIQUE">Unique key</option>
                    </select>
                </div>
                <div class="input-field col s2" style="top: 13px">
                        <span class="material-icons remove_line" data-line="1" style="cursor: pointer" onclick="deleteRow(1)">clear</span>
                </div>
            </div>
        </div>
        <div class="col s4">

        </div>
        <div class="col s8">
            <button class="btn waves-effect hover waves-light left" id="add_line"><span>Add new row</span></button>
            <button id="table_add" class="btn btn-success right red">Create table</button>
        </div>

    </div>
</div>

<?php
    include("assets/includes/footer.php")
?>