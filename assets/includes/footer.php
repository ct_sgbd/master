        </main>

        <footer class="indigo lighten-3 page-footer">
                <div class="container ">
                    <div class="row center">
                        <h5 class="white-text">Project realised by:</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="https://www.linkedin.com/in/mardare-cristian-931273123/">Mardare Cristian</a> - 3rd year, Group 1</li>
                        </ul>
                    </div>
                </div>
                <div class="footer-copyright #7986cb indigo lighten-2 center">
                    <div class="container">
                        © 2017 Copyright Ovidius University, Faculty of Mathematics and Informatics
                    </div>
                </div>
        </footer>
    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <script src="assets/js/main.js"></script>

</html>