<?php

class database extends SQLite3
{
    function __construct($file)
    {
        $this->open("../db/".$file.".db");
    }

}

$db_name = $_POST['db_name'];

$db = new database($db_name);
if(!$db){
    echo $db->lastErrorMsg();
} else {
//    echo "Opened database successfully\n";
}

$table_name = $_POST['table_name'];
$data_array = $_POST['data_array'];
$columns = "";
$values = "";

$last_key = end(array_keys($data_array));
foreach ($data_array as $key => $data){
    if($key != $last_key) {
        $columns .= $data['name'] . ",";
        if(gettype($data['value']) == "string") {
            $values .="\"".$data['value'] ."\",";
        } else {
            $values .= $data['value'] . ",";
        }
    } else {
        $columns .= $data['name'];
        if(gettype($data['value']) == "string") {
            $values .="\"".$data['value'] ."\"";
        } else {
            $values .= $data['value'];
        }
    }
}

$ret = $db->query("INSERT INTO ".$table_name." (".$columns.")
      VALUES (".$values.")");

if(!$ret){
    echo $db->lastErrorMsg();
} else {
    echo "true";
}

$db->close();