<?php

class database extends SQLite3
{
    function __construct($file)
    {
        $this->open("db/".$file);
    }

}

$db_name = $_GET['db'];
$table_name = $_GET['table'];

$db = new database($db_name);
if(!$db){
    echo $db->lastErrorMsg();
} else {
//    echo "Opened database successfully\n";
}

$ret = $db->query("PRAGMA table_info(".$table_name.")");

$columns = [];

if(!$ret){
    echo $db->lastErrorMsg();
} else {
    while($data = $ret->fetchArray(SQLITE3_ASSOC) ) {
        array_push($columns, $data);
    }
}

$db->close();

$db2 = new database($db_name);
if (!$db2) {
    echo $db2->lastErrorMsg();
} else {
//    echo "Opened database successfully\n";
}

$ret = $db2->query("SELECT * FROM " . $table_name);

$table_data = [];

if (!$ret) {
    echo $db2->lastErrorMsg();
} else {
    while ($data = $ret->fetchArray(SQLITE3_ASSOC)) {
        array_push($table_data, $data);
    }
}

$db2->close();

if(isset($_GET['id'])) {
    $id = $_GET['id'];

    $db3 = new database($db_name);
    if (!$db3) {
        echo $db3->lastErrorMsg();
    } else {
//    echo "Opened database successfully\n";
    }

    $ret = $db3->query("SELECT * FROM " . $table_name ." WHERE ID = \"" .$id. "\"");

    $column_data = [];

    if (!$ret) {
        echo $db3->lastErrorMsg();
    } else {
        while ($data_column = $ret->fetchArray(SQLITE3_ASSOC)) {
            array_push($column_data, $data_column);
        }
    }

    $db3->close();
}

$db4 = new database($db_name);
if (!$db4) {
    echo $db4->lastErrorMsg();
} else {
//    echo "Opened database successfully\n";
}

$ret = $db4->query("PRAGMA INDEX_LIST(".$table_name.")");

$table_info = [];

if (!$ret) {
    echo $db4->lastErrorMsg();
} else {
    while ($data = $ret->fetchArray(SQLITE3_ASSOC)) {
        array_push($table_info, $data);
    }
}

$db4->close();
