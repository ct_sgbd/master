<?php

class database extends SQLite3
{
    function __construct($file)
    {
        $this->open("../db/".$file.".db");
    }

}

$db_name = $_POST['db_name'];
$table_name = $_POST['table_name'];

$db = new database($db_name);
if (!$db) {
    echo $db->lastErrorMsg();
} else {
//    echo "Opened database successfully\n";
}


$status = $_POST['status'];

if($status == "edit") {

    $id = $_POST['id'];
    $data_array = $_POST['data_array'];
    $columns = "";
    $values = "";

    foreach ($data_array as $key => $data) {
        if (gettype($data['value']) == "string") {

            $ret = $db->query("UPDATE ". $table_name. " SET ".$data['name']."=\"".$data['value']."\" WHERE ID=".$id);

        } else {

            $ret = $db->query("UPDATE ". $table_name. " SET ".$data['name']."=".$data['value']." WHERE ID=".$id);

        }
    }

} elseif($status == "delete"){

    $id = $_POST['id'];

    $ret = $db->query("DELETE from ". $table_name . " WHERE ID=".$id);
}


if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "true";
}

$db->close();