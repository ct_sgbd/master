<?php

class secondDB extends SQLite3
{
    function __construct($file)
    {
        $this->open("../db/".$file.".db");
    }

}

$db_name = $_POST['db_name'];

$db = new secondDB($db_name);
if(!$db){
    echo $db->lastErrorMsg();
} else {
//    echo "Opened database successfully\n";
}

$status = $_POST['status'];
$table_name = $_POST['table_name'];

if($status == "edit") {

    $new_table_name = $_POST['new_table_name'];

    $ret = $db->query("ALTER TABLE ".$table_name." RENAME TO ". $new_table_name);

    if(!$ret){
        echo $db->lastErrorMsg();
    } else {
        echo $new_table_name;
    }


} elseif($status == "delete"){

    $ret = $db->query("DROP TABLE ". $table_name);

    if (!$ret) {
        echo $db->lastErrorMsg();
    } else {
        echo "true";
    }

} elseif($status == "empty") {

    $ret = $db->query("DELETE FROM " . $table_name);
    $ret = $db->query("VACUUM");

    if (!$ret) {
        echo $db->lastErrorMsg();
    } else {
        echo "true";
    }
}

$db->close();
