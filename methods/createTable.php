<?php

class secondDB extends SQLite3
{
    function __construct($file)
    {
        $this->open("../db/".$file);
    }

}

$db_name = $_POST['db_name'];

$db = new secondDB($db_name);
if(!$db){
    echo $db->lastErrorMsg();
} else {
    echo "Opened database successfully\n";
}

$table_name = $_POST['table_name'];
$array_name = $_POST['array_name'];
$array_type = $_POST['array_type'];
$array_length = $_POST['array_length'];
$array_default = $_POST['array_default'];
$array_extra = $_POST['array_extra'];

$prepare_array = [];

for($i=0;$i<count($array_name);$i++){
    for($j=0;$j<count($array_name);$j++) {
        if($i == $j) {
            $prepare_array[$i][] = $array_name[$j];
            $prepare_array[$i][] = $array_type[$j];
            $prepare_array[$i][] = $array_length[$j];
            $prepare_array[$i][] = $array_default[$j];
            $prepare_array[$i][] = $array_extra[$j];
        }
    }
}
$query_string = "";

$last_key = end(array_keys($prepare_array));
foreach($prepare_array as $key => $item) {
    if($item[4] == "NONE"){
        $item[4] = "";
    }

    if($key != $last_key) {
        $query_string .= $item[0] . " " . $item[1] . "(" . $item[2] . ")" . " ". $item[4] . " " . $item[3] . ",";
    } else {
        $query_string .= $item[0] . " " . $item[1] . "(" . $item[2] . ")" . " ". $item[4] . " " . $item[3];
    }
}

$ret = $db->query("CREATE TABLE ". $table_name ."(
    ID INTEGER PRIMARY KEY NULL,
    ". $query_string . "
    )");

if(!$ret){
    echo $db->lastErrorMsg();
} else {
    echo "Success";
}

$db->close();



