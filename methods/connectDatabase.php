<?php
class MyDB extends SQLite3
{
    function __construct($file)
    {
        $this->open("db/".$file.".db");
    }

}

function getTables($db_name){
    $db = new MyDB($db_name);
    if(!$db){
        echo $db->lastErrorMsg();
    } else {
//        echo "Opened database successfully\n";
    }

    $ret = $db->query("SELECT * FROM sqlite_master where type='table'");

//    $ret = $db->exec($sql);

    $data = [];

    if(!$ret){
        array_push($data,$db->lastErrorMsg());
    } else {
        while($table = $ret->fetchArray(SQLITE3_ASSOC) ) {
            array_push($data, $table['name']);
        }
    }

    $db->close();
    return $data;
}

