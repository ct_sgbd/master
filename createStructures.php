<?php
    include("assets/includes/header.php");
    include('methods/getStructures.php');

?>

    <div class="container">
        <div class="row" >
            <div class="col s4" id="jstree"><?php listFolderFiles("db") ?></div>

            <div class="col s8 container">
                <input id="db_name" value="<?= substr($_GET["db"], 0, -3) ?>" style="display: none;">
                <input id="table_name" value="<?= $_GET["table"] ?>" style="display: none;">
                <h3>Create new data</h3>

                <?php
                foreach ($columns as $column){
                    if($column['name'] != "ID") {

                        $default = "";
                        if($column["notnull"] == 0){
                            $default = "NULL";
                        } else {
                            $default = "NOT NULL";
                        }

                        echo"<div>";
                            echo "<div class=\"input-field col s6\">";
                                echo "<label for=".$column['name'].">".$column['name']."</label>";
                                echo "<input id=".$column['name']." data-name=".$column['name']." data-type=".$column['type']." data-default=".$default." type=\"text\" class=\"validate data\" >";
                            echo "</div>";
                            echo "<div class='col s6 card cyan lighten-5'>
                                    <div class='card-content'><b>Column data type: </b>".$column['type']."</div>
                                  </div>";
                        echo"</div>";
                        }
                    }
                ?>
            </div>
            <div class="col s4"></div>
            <div class="col s8">
                <button id="back" class="btn btn-success left cyan">Go Back</button>
                <button id="add_data" class="btn btn-success right red">Create data</button>
            </div>

        </div>
    </div>

<?php
    include("assets/includes/footer.php")
?>